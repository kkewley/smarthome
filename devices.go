package smarthome

type Trait string
type Device string
type InputMode string
type OutputType string

const (
	TraitBrightness         Trait = "action.devices.traits.Brightness"
	TraitCameraStream       Trait = "action.devices.traits.CameraStream"
	TraitColorSpectrum      Trait = "action.devices.traits.ColorSpectrum"
	TraitColorTemperature   Trait = "action.devices.traits.ColorTemperature"
	TraitDock               Trait = "action.devices.traits.Dock"
	TraitModes              Trait = "action.devices.traits.Modes"
	TraitOnOff              Trait = "action.devices.traits.OnOff"
	TraitRunCycle           Trait = "action.devices.traits.RunCycle"
	TraitScene              Trait = "action.devices.traits.Scene"
	TraitStartStop          Trait = "action.devices.traits.StartStop"
	TraitTemperatureSetting Trait = "action.devices.traits.TemperatureSetting"
	TraitToggles            Trait = "action.devices.traits.Toggles"
)

const (
	DeviceCamera       Device = "action.devices.types.CAMERA"
	DeviceDishwasher   Device = "action.devices.types.DISHWASHER"
	DeviceDryer        Device = "action.devices.types.DRYER"
	DeviceLight        Device = "action.devices.types.LIGHT"
	DeviceOutlet       Device = "action.devices.types.OUTLET"
	DeviceRefrigerator Device = "action.devices.types.REFRIGERATOR"
	DeviceScene        Device = "action.devices.types.SCENE"
	DeviceSwitch       Device = "action.devices.types.SWITCH"
	DeviceThermostat   Device = "action.devices.types.THERMOSTAT"
	DeviceVacuum       Device = "action.devices.types.VACUUM"
	DeviceWasher       Device = "action.devices.types.WASHER"
	DeviceSensor       Device = "action.devices.types.SENSOR"
)

const (
	InputModeSimple InputMode = "input.SIMPLE"
	InputModeJSON   InputMode = "input.JSON"
)

const (
	OutputTypeStatus OutputType = "output.Status"
	OutputTypeData   OutputType = "output.Data"
)
