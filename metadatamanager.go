package smarthome

type DeviceData struct {
	ID              string     `json:"id,omitempty" validate:"nonzero"`
	Type            Device     `json:"type" validate:"nonzero"`
	Traits          []Trait    `json:"traits,omitempty"`
	Name            DeviceName `json:"name" validate:"nonzero"`
	WillReportState bool       `json:"willReportState"`
	DeviceInfo      DeviceInfo `json:"deviceInfo,omitempty"`
	CustomData      CustomData `json:"customData,omitempty"`
}

type DeviceName struct {
	DefaultNames []string `json:"defaultNames",omitempty`
	Name         string   `json:"name" validate:"nonzero"`
	Nicknames    []string `json:"nicknames,omitempty"`
}

type DeviceInfo struct {
	Manufacturer string `json:"manufacturer,omitempty"`
	Model        string `json:"model,omitempty"`
	HwVersion    string `json:"hwVersion,omitempty"`
	SwVersion    string `json:"swVersion,omitempty"`
}

type CustomData struct {
	Outputs []Output `json:"outputs,omitempty"`
	Inputs  []Input  `json:"input,omitempty"`
}

type Output struct {
	Name                           string     `json:"name" validate:"nonzero"`
	Description                    string     `json:"description,omitempty"`
	SnapshotTopic                  string     `json:"snapshotTopic" validate:"nonzero"`
	DeviceManagerHistoricalRequest string     `json:"deviceManagerHistoricalRequest,omitempty`
	ShouldArchive                  bool       `json:"shouldArchive,omitempty"`
	OutputType                     OutputType `json:"outputType,omitempty"`
}

type Input struct {
	Name        string    `json:"name" validate:"nonzero"`
	Description string    `json:"description,omitempty"`
	InputTopic  string    `json:"snapshotTopic" validate:"nonzero"`
	InputMode   InputMode `json:"inputMode,omitempty"`
	InputTrait  Trait     `json:"inputTrait,omitempty"`
}
